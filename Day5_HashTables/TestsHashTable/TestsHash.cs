namespace TestsHashTable
{

    [TestClass]
    public class Task1
    {
        [TestMethod]
        public void findCoolestPlate()//max letters in a numberplate
        {
            CarPlate.CarPlateToOwner.Add("PB5839TX", "Georgi Georgiev");
            CarPlate.CarPlateToOwner.Add("CB8888CB", "Stefan Gerdjikov");
            CarPlate.CarPlateToOwner.Add("EB5849AH", "Blagovest Evlogiev");
            CarPlate.CarPlateToOwner.Add("CB7B7B7B", "Bojidar Serafimov");

            KeyValuePair<string, string> expected =
                new KeyValuePair<string, string>("CB7B7B7B", "Bojidar Serafimov");
            KeyValuePair<string, string> result = CarPlate.returnCoolestNumberPlate();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void findTheMultiCarOwner()//max letters in a numberplate
        {
            CarPlate.CarPlateToOwner.Add("CA7B7B7B", "Bojidar Serafimov");

            string expected = "Bojidar Serafimov";
            string result = CarPlate.getTheOwnerWithMostCars();
            Assert.AreEqual(expected, result);
        }

    }

    [TestClass]
    public class Task3
    {
        [TestMethod]
        public void TestGivenSetOfChars1()
        {
            string input = "Hell world";
            List<char> set = new List<char> { 'H', 'e', 'l', 'w', 'o', 'r', 'd' };
            List<char> result = NonRepeatingChars.getSetOfChars(input);
            CollectionAssert.AreEquivalent(set, result);
        }

        [TestMethod]
        public void TestGivenSetOfChars2()
        {
            string input = "lllllll";
            List<char> set = new List<char> { 'l' };
            List<char> result = NonRepeatingChars.getSetOfChars(input);
            CollectionAssert.AreEquivalent(set, result);
        }

    }

    [TestClass]
    public class Task2
    {
        [TestMethod]
        public void TestGivenSetOfChars1()
        {
            int[] arr = { 1, 2, 3, 4, 4 };
            int[] arr1 = { 2, 2, 4, 4 };
            int[] expected = { 2, 4 };
            int[] result = IntersectionOfArrays.extractIntersection(arr1, arr);
            
            CollectionAssert.AreEquivalent(expected,result);
        }

    }
}
