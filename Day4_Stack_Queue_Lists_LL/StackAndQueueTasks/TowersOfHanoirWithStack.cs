﻿
public static class TowersOfHanoirWithStack
{

    static Stack<int> source = new();
    static Stack<int> middle = new();
    static Stack<int> destination = new();

    static char start = 'A';
    static char mid = 'B';
    static char end = 'C';

    static void Main()
    {
        Console.WriteLine("Enter the number of disks: ");
        int n = int.Parse(Console.ReadLine());

        for (int i = n; i > 0; i--)
        {
            source.Push(i);
        }

        SolveTowersOfHanoi(n, source, middle, destination, start,mid,end);
    }

    internal static void SolveTowersOfHanoi(int n, Stack<int> source, Stack<int> middle, Stack<int> destination,
        char start, char mid, char end)
    {
        if (n > 0)
        {
            SolveTowersOfHanoi(n - 1, source, destination, middle, start, end, mid);

            destination.Push(source.Pop());
            Console.WriteLine($"Move disk {n} from {start} to {end}");

            SolveTowersOfHanoi(n - 1, middle, source, destination, mid, start, end);
        }
    }
}

