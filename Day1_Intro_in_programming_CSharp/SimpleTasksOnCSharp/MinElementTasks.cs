﻿
public static class MinElementTasks
{
    public static int findMinIndexOfElement(int[] arr)
    {
        int index = 0;
        int minElem = Int32.MaxValue;
        for (int i = 0; i < arr.Length; i++)
        {
            if (minElem > arr[i])
            {
                minElem = arr[i];
                index=i;
            }
        }

        return index;
    }

    public static int extractKthMinElement(int[]arr, int kthElem)
    {
        if(kthElem < 0 || kthElem>arr.Length)
        {
            return -1;
        }

        List<int> copyArr = new();
        foreach (var item in arr)
        {
            copyArr.Add(item);
        }
        copyArr.Sort();
        return copyArr[kthElem];
    }

}

